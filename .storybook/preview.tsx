import * as React from 'react'
import { ThemeProvider } from '../src'
import { StyledEngineProvider } from '@mui/material/styles'

const withMuiTheme = (Story) => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider>
      <Story />
    </ThemeProvider>
  </StyledEngineProvider>
)

export const decorators = [withMuiTheme]

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    expanded: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
