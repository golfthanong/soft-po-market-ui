import React from 'react'
import {
  createTheme,
  ThemeOptions,
  ThemeProvider as MUIThemeProvider,
} from '@mui/material/styles'
import palette from './palette'
import typography from './typography'
import { StylesBaseline } from './globalStyles'
import { componentsStyle } from './componentsStyle'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import {
  experimental_extendTheme as extendTheme,
  Experimental_CssVarsProvider as CssVarsProvider,
} from '@mui/material'

enum ThemeMode {
  Light = 'light',
  Dark = 'dark',
}
type ThemeProps = {
  themeMode: ThemeMode
}

function theme({ themeMode }: ThemeProps) {
  const themeDirection = 'ltr'
  const themeOptions: ThemeOptions = {
    palette: palette(themeMode),
    typography: { ...typography },
    shape: { borderRadius: 8 },
    direction: themeDirection,
    components: componentsStyle,
  }
  return createTheme(themeOptions)
}

type Props = {
  children: React.ReactNode
  themeMode?: ThemeMode
}

/**
 * Provides the theme for the UI components library.
 *
 * @param children - The child components to be wrapped by the theme provider.
 * @param themeMode - The mode of the theme (default: ThemeMode.Light).
 * @returns The themed UI components.
 */
export function ThemeProvider({
  children,
  themeMode = ThemeMode.Light,
}: Props) {
  const themeOptions: ThemeOptions = theme({ themeMode })
  const _theme = createTheme(themeOptions)
  const _themeVariable = extendTheme({
    ...themeOptions,
  })
  return (
    <MUIThemeProvider theme={_theme}>
      <CssVarsProvider theme={_themeVariable}>
        <StylesBaseline />
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          {children}
        </LocalizationProvider>
      </CssVarsProvider>
    </MUIThemeProvider>
  )
}

export const themes = {
  light: theme({ themeMode: ThemeMode.Light }),
  dark: theme({ themeMode: ThemeMode.Dark }),
}
