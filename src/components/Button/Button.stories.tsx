import React from 'react'
import type { Meta, StoryFn } from '@storybook/react'
import { Button, ButtonProps } from '.'

export default {
  title: 'components/Button',
  component: Button,
  argTypes: {
    startIcon: {
      control: 'object',
    },
  },
  parameters: {
    controls: { expanded: true },
  },
} satisfies Meta<typeof Button>

const Template: StoryFn<ButtonProps> = (args) => {
  return <Button {...args}>{'button'}</Button>
}

export const Contained = Template.bind({})
Contained.args = {
  variant: 'contained',
}

export const Outlined = Template.bind({})
Outlined.args = {
  variant: 'outlined',
}

export const Text = Template.bind({})
Text.args = {
  variant: 'text',
}
