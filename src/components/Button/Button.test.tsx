import { render, screen } from '@testing-library/react'
import { Button, ButtonProps } from '.'

describe('Button Unit Test', () => {
  const mockProps: ButtonProps = {}

  const beforeEachTest = (props = mockProps) =>
    render(<Button {...props} />, {})

  it('Should render button', () => {
    beforeEachTest()
    const btn = screen.getByTestId('my-button')
    expect(btn).toBeInTheDocument()
  })

  it('UI should not change', () => {
    const comp = beforeEachTest()
    expect(comp).toMatchSnapshot()
  })
})
