/// <reference types="vitest" />
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './src/setupTests.ts',
    root: './src',
    coverage: {
      reporter: ['text', 'json', 'html'],
      exclude: ['**/*.d.ts', 'theme/**', 'stories/**'],
    },
  },
})
