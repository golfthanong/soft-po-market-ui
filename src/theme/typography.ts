declare module '@mui/material/styles' {
  interface TypographyVariants {
    caption1_bold: React.CSSProperties
    caption1: React.CSSProperties
    caption2: React.CSSProperties
  }

  interface TypographyVariantsOptions {
    caption1_bold?: React.CSSProperties
    caption1?: React.CSSProperties
    caption2?: React.CSSProperties
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    caption1_bold: true
    caption1: true
    caption2: true
  }
}

export function remToPx(value: string) {
  return Math.round(parseFloat(value) * 16)
}

export function pxToRem(value: number) {
  return `${value / 16}rem`
}

export function responsiveFontSizes({
  sm,
  md,
  lg,
}: {
  sm: number
  md: number
  lg: number
}) {
  return {
    '@media (min-width:600px)': {
      fontSize: pxToRem(sm),
    },
    '@media (min-width:900px)': {
      fontSize: pxToRem(md),
    },
    '@media (min-width:1200px)': {
      fontSize: pxToRem(lg),
    },
  }
}

export const FONT_PRIMARY = 'Noto_sans'
export const FONT_PRIMARY_LOOPED = 'Noto_sans'

const typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  h1: {
    fontWeight: 700,
    fontSize: '2.986rem',
    lineHeight: 1.4,
  },
  h2: {
    fontWeight: 700,
    fontSize: '2.488rem',
    lineHeight: 1.4,
  },
  h3: {
    fontWeight: 700,
    fontSize: '2.074rem',
    lineHeight: 1.4,
  },
  h4: {
    fontWeight: 700,
    fontSize: '1.728rem',
    lineHeight: 1.4,
    ...responsiveFontSizes({ sm: 20, md: 24, lg: 24 }),
  },
  h5: {
    fontWeight: 700,
    fontSize: '1.440rem',
    lineHeight: 1.4,
  },
  h6: {
    fontWeight: 700,
    fontSize: '1.2rem',
    lineHeight: 1.4,
  },
  subtitle1: {
    fontWeight: 600,
    fontSize: '1.2rem',
    lineHeight: 1.4,
  },
  subtitle2: {
    fontWeight: 600,
    fontSize: '1.000rem',
    lineHeight: 1.4,
  },
  body1: {
    fontFamily: FONT_PRIMARY_LOOPED,
    fontWeight: 400,
    fontSize: '1.000rem',
    lineHeight: 1.4,
  },
  body2: {
    fontFamily: FONT_PRIMARY_LOOPED,
    fontWeight: 400,
    fontSize: '0.833rem',
    lineHeight: 1.4,
  },
  overline: {
    fontWeight: 600,
    fontSize: '0.75rem',
    lineHeight: 1.333,
    textTransform: 'none',
  },
  button: {
    fontWeight: 700,
    lineHeight: 24 / 14,
    fontSize: pxToRem(14),
    textTransform: 'capitalize',
  },
  caption1_bold: {
    fontFamily: FONT_PRIMARY,
    fontWeight: 700,
    fontSize: '0.833rem',
    lineHeight: 1.4,
  },
  caption1: {
    fontFamily: FONT_PRIMARY,
    fontWeight: 500,
    fontSize: '0.833rem',
    lineHeight: 1.4,
  },
  caption2: {
    fontFamily: FONT_PRIMARY,
    fontWeight: 500,
    fontSize: '0.694rem',
    lineHeight: 1.4,
  },
} as const

export default typography
