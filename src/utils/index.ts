import numeral from 'numeral'

export const formatPriceNumber = (price: number, format?: string) => {
  if (format) {
    return numeral(price).format(format)
  }
  if (price <= 0) {
    return numeral(price).format('0.000000')
  }
  return numeral(price).format('0,0.00')
}
