import { Color } from '@mui/material'
import { alpha } from '@mui/material/styles'
import { PaletteColor, PaletteColorOptions } from '@mui/material/styles'
import { ColorPartial } from '@mui/material/styles/createPalette'

export type ColorSchema =
  | 'primary'
  | 'secondary'
  | 'analogous'
  | 'red'
  | 'orange'
  | 'green'
  | 'slate_grey'
  | 'info'
  | 'success'
  | 'warning'
  | 'error'

declare module '@mui/material/styles' {
  interface Palette {
    analogous?: PaletteColor
    red?: PaletteColor
    orange?: PaletteColor
    green?: PaletteColor
    slate_grey?: PaletteColor
    primaryColor?: Color
  }
  interface PaletteOptions {
    analogous?: PaletteColorOptions
    red?: PaletteColorOptions
    orange?: PaletteColorOptions
    green?: PaletteColorOptions
    slate_grey?: PaletteColorOptions
    primaryColor?: ColorPartial
  }
}

declare module '@mui/material/styles/createPalette' {
  interface TypeBackground {
    neutral: string
  }
  interface SimplePaletteColorOptions {
    lighter: string
    darker: string
  }
  interface PaletteColor {
    lighter: string
    darker: string
  }
}

const GREY = {
  0: '#FFFFFF',
  50: '#eeeeee',
  100: '#d5d6d7',
}

const PRIMARY_COLOR = {
  0: '#FFFFFF',
  50: '#eaeaf8',
  100: '#cacbed',
}

export const PRIMARY = {
  main: PRIMARY_COLOR[100],
  light: PRIMARY_COLOR[100],
  lighter: PRIMARY_COLOR[100],
  dark: PRIMARY_COLOR[100],
  darker: PRIMARY_COLOR[100],
  hover: PRIMARY_COLOR[100],
  contrastText: PRIMARY_COLOR[0],
}

const SECONDARY = {
  main: '#fff',
  light: '#ffff',
  lighter: '#fff',
  dark: '#fff',
  darker: '#fff',
  contrastText: '#fff',
}

const ANALOGOUS_COLOR = {
  50: '#ebeafd',
  100: '#cccbfa',
}

const ANALOGOUS = {
  main: ANALOGOUS_COLOR[100],
  light: ANALOGOUS_COLOR[100],
  lighter: ANALOGOUS_COLOR[100],
  dark: ANALOGOUS_COLOR[100],
  darker: ANALOGOUS_COLOR[100],
  contrastText: '#FFFFFF',
  ...ANALOGOUS_COLOR,
}

const RED_COLOR = {
  50: '#ffebee',
  100: '#ffcdd2',
}

const RED = {
  main: RED_COLOR[100],
  light: RED_COLOR[100],
  lighter: RED_COLOR[50],
  dark: RED_COLOR[100],
  darker: RED_COLOR[100],
  contrastText: '#FFFFFF',
  ...RED_COLOR,
}

const ORANGE_COLOR = {
  50: '#fff3e0',
  100: '#ffe0b2',
}

const ORANGE = {
  main: ORANGE_COLOR[100],
  light: ORANGE_COLOR[100],
  lighter: ORANGE_COLOR[50],
  dark: ORANGE_COLOR[100],
  darker: ORANGE_COLOR[100],
  contrastText: '#FFFFFF',
  ...ORANGE_COLOR,
}

const GREEN_COLOR = {
  50: '#e8f5e9',
  100: '#c8e6c9',
}

const GREEN = {
  main: GREEN_COLOR[100],
  light: GREEN_COLOR[100],
  lighter: GREEN_COLOR[50],
  dark: GREEN_COLOR[100],
  darker: GREEN_COLOR[100],
  contrastText: '#FFFFFF',
  ...GREEN_COLOR,
}

const BLUE_COLOR = {
  50: '#e3f2fd',
  100: '#bbdefb',
}

const BLUE = {
  main: BLUE_COLOR[100],
  light: BLUE_COLOR[100],
  lighter: BLUE_COLOR[100],
  dark: BLUE_COLOR[100],
  darker: BLUE_COLOR[100],
  contrastText: '#FFFFFF',
  ...BLUE_COLOR,
}

const SLATE_GRAY = {
  main: GREY[100],
  light: GREY[100],
  lighter: GREY[100],
  dark: GREY[100],
  darker: GREY[100],
}

const INFO = {
  main: '#fff',
  light: '#fff',
  lighter: '#fff',
  dark: '#fff',
  darker: '#fff',
  contrastText: '#FFFFFF',
}

const SUCCESS = {
  main: GREEN_COLOR[100],
  light: GREEN_COLOR[100],
  lighter: '#c8e6c9',
  dark: GREEN_COLOR[100],
  darker: GREEN_COLOR[100],
  background: GREEN_COLOR[50],
  contrastText: '#000000',
}

const WARNING = {
  main: ORANGE_COLOR[100],
  light: ORANGE_COLOR[100],
  lighter: '#ffe0b2',
  dark: ORANGE_COLOR[100],
  darker: ORANGE_COLOR[100],
  background: ORANGE_COLOR[50],
  contrastText: '#000000',
}

const ERROR = {
  main: RED_COLOR[100],
  light: RED_COLOR[100],
  lighter: '#ffcdd2',
  dark: RED_COLOR[100],
  darker: RED_COLOR[100],
  background: '#FFEBEE',
  contrastText: '#FFFFFF',
}

const COMMON = {
  common: { black: '#000', white: '#fff' },
  primary: PRIMARY,
  secondary: SECONDARY,
  analogous: ANALOGOUS,
  red: RED,
  orange: ORANGE,
  green: GREEN,
  blue: BLUE,
  slate_grey: SLATE_GRAY,
  info: INFO,
  success: SUCCESS,
  warning: WARNING,
  error: ERROR,
  grey: GREY,
  primaryColor: PRIMARY_COLOR,
  divider: alpha(GREY[100], 0.24),
  action: {
    hover: alpha(GREY[100], 0.08),
    selected: alpha(GREY[100], 0.16),
    disabled: alpha(GREY[100], 0.8),
    disabledBackground: alpha(GREY[100], 0.24),
    focus: alpha(GREY[100], 0.24),
    hoverOpacity: 0.08,
    disabledOpacity: 0.48,
  },
}

export default function palette(themeMode: 'light' | 'dark') {
  const light = {
    ...COMMON,
    primary: PRIMARY,
    mode: 'light',
    text: {
      primary: GREY[100],
      secondary: GREY[100],
      disabled: GREY[100],
      light: GREY[100],
      dark: GREY[100],
    },
    background: { paper: '#fff', default: '#F8FAFF', neutral: GREY[100] },
    action: {
      ...COMMON.action,
      active: GREY[100],
    },
  } as const

  const dark = {
    ...COMMON,
    primary: PRIMARY,
    mode: 'dark',
    text: {
      primary: '#fff',
      secondary: GREY[100],
      disabled: GREY[100],
    },
    background: {
      paper: GREY[100],
      default: GREY[100],
      neutral: alpha(GREY[100], 0.16),
    },
    action: {
      ...COMMON.action,
      active: GREY[100],
    },
  } as const

  return themeMode === 'light' ? light : dark
}
